﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SistemaBebidas.Classes
{
    public  class Usuario
        
    {
        public int Id { get; set; }
        public string Nome { get; set; }
        public string Email { get; set; }
        public string Senha { get; set; }
        

        public Usuario() 
        { 

        }
        
        public virtual string ConsultarUsuario()
        {
            return Id + " - " +Nome+ " - "+Email;
        }

        public string GerarHash(string Senha)
        {

             using (MD5 md5Hash = MD5.Create())
            {
                 return RetonarHash(md5Hash, Senha);
            }
        }


       

        
    }
}
